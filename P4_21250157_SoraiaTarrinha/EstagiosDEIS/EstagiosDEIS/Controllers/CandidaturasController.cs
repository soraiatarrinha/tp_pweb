﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstagiosDEIS.Models;
using Microsoft.AspNet.Identity;

namespace EstagiosDEIS.Controllers
{[Authorize]
    public class CandidaturasController : Controller
    {
        private EstagiosContext db = new EstagiosContext();

        // GET: Candidaturas
        public ActionResult Index()
        {
            string userIdA = User.Identity.GetUserId();


            Aluna aluna = db.Alunas.SingleOrDefault(x => x.UserId == userIdA);

            
            return View(db.Candidaturas.Where(c=>c.AlunaId.Equals(aluna.alunoId)).ToList());
        }

        public ActionResult ListaCandidaturas()
        {
            var candidaturas = db.Candidaturas.Include(c => c.Aluna).Include(c => c.Proposta);
            return View(candidaturas.ToList());
        }

        public ActionResult ListaCandidaturasEstagios()
        {
            var candidaturas = db.Candidaturas.Include(c => c.Aluna).Include(c => c.Proposta);
            return View(candidaturas.ToList());
        }


        // GET: Candidaturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Candidatura candidatura = db.Candidaturas.Find(id);
            if (candidatura == null)
            {
                return HttpNotFound();
            }
            return View(candidatura);
        }
        [Authorize(Roles ="Aluno")]
        // GET: Candidaturas/Create
        public ActionResult Create(int ?id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Proposta proposta = db.Propostas.Find(id);


            if (proposta == null)
                return HttpNotFound();
      
            
           
      
            
            
            return View();
        }

        // POST: Candidaturas/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "candidaturaId,AlunaId,PropostaId,estado")] Candidatura candidatura,int id)
        {
            var conta = (from p in db.Propostas
                         join c in db.Candidaturas on p.propostaId equals c.PropostaId
                         select c.candidaturaId).Count();


          
            string userIdA = User.Identity.GetUserId();


            Aluna aluna = db.Alunas.SingleOrDefault(x => x.UserId == userIdA);
            candidatura.AlunaId= aluna.alunoId;
            Session["AlunaId"] = candidatura.AlunaId.ToString();

            candidatura.PropostaId = id;
            var num_candidaturas = from p in db.Propostas
                                where candidatura.PropostaId == p.propostaId
                                select p.NumeroCandidaturas;

           



            

            if (conta < num_candidaturas.First()) {

                Candidatura cand = new Candidatura();
                cand.AlunaId = candidatura.AlunaId;














                cand.PropostaId = candidatura.PropostaId;
                cand.estado = false;


                if (ModelState.IsValid)
                {
                    db.Candidaturas.Add(cand);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(cand);
            }
            return View();
        }

        // GET: Candidaturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Candidatura candidatura = db.Candidaturas.Find(id);
            if (candidatura == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlunaId = new SelectList(db.Alunas, "alunoId", "Nome", candidatura.AlunaId);
            ViewBag.PropostaId = new SelectList(db.Propostas, "propostaId", "propostaId", candidatura.PropostaId);
            return View(candidatura);
        }

        // POST: Candidaturas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "candidaturaId,AlunaId,PropostaId,estado")] Candidatura candidatura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(candidatura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlunaId = new SelectList(db.Alunas, "alunoId", "Nome", candidatura.AlunaId);
            ViewBag.PropostaId = new SelectList(db.Propostas, "propostaId", "propostaId", candidatura.PropostaId);
            return View(candidatura);
        }

        // GET: Candidaturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Candidatura candidatura = db.Candidaturas.Find(id);
            if (candidatura == null)
            {
                return HttpNotFound();
            }
            return View(candidatura);
        }

        // POST: Candidaturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Candidatura candidatura = db.Candidaturas.Find(id);
            db.Candidaturas.Remove(candidatura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult AprovarCandidatura(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Candidatura candidatura= db.Candidaturas.Find(id);


            if (candidatura == null)
                return HttpNotFound();


            return View(candidatura);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AprovarCandidatura([Bind(Include = "candidaturaId,AlunaId,PropostaId,estado")] Candidatura candidatura)
        {
     
            candidatura.estado = true;

            Proposta proposta = db.Propostas.Find(candidatura.PropostaId);

            proposta.CandidaturasAprovadas++;
            


                if (ModelState.IsValid)
                {
                db.Entry(candidatura).State = EntityState.Modified;
                db.SaveChanges();
                    return RedirectToAction("ListaCandidaturas");
                }
                return View(candidatura);
           
        }

        public ActionResult RecusarCandidatura(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Candidatura candidatura = db.Candidaturas.Find(id);


            if (candidatura == null)
                return HttpNotFound();


            return View(candidatura);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecusarCandidatura([Bind(Include = "candidaturaId,AlunaId,PropostaId,estado")] Candidatura candidatura,int id)
        {



            Candidatura cand = db.Candidaturas.Find(id);

            db.Candidaturas.Remove(cand);
            db.SaveChanges();
            RedirectToAction("ListaCandidaturas");
            return View(candidatura);

        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

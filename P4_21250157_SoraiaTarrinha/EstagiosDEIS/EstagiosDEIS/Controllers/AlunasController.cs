﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstagiosDEIS.Models;
using Microsoft.AspNet.Identity;

namespace EstagiosDEIS.Controllers
{
    public class AlunasController : Controller
    {
        private EstagiosContext db = new EstagiosContext();

        // GET: Alunas
        public ActionResult Index() {


            string userIdA = User.Identity.GetUserId();


            return View(db.Alunas.Where(a=> a.UserId.Equals(userIdA)).ToList());
        }

        // GET: Alunas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluna aluna = db.Alunas.Find(id);
            if (aluna == null)
            {
                return HttpNotFound();
            }
            return View(aluna);
        }

        // GET: Alunas/Create
        public ActionResult Create()
        {
       
            return View();
        }

        // POST: Alunas/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "alunoId,Nome,Contacto,Ano,Ramo,Disciplinas,Notas,num_candidaturas,UserId")] Aluna aluna)
        {
            string userIdA = User.Identity.GetUserId();
            aluna.UserId = userIdA;
            aluna.num_candidaturas = 0;
            Session["alunoId"] = aluna.alunoId;
            if (ModelState.IsValid)
            {
                db.Alunas.Add(aluna);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

          
            return View(aluna);
        }

        // GET: Alunas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluna aluna = db.Alunas.Find(id);
            if (aluna == null)
            {
                return HttpNotFound();
            }
          
            return View(aluna);
        }

        // POST: Alunas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "alunoId,Nome,Contacto,Ano,Ramo,Disciplinas,Notas,num_candidaturas,UserId")] Aluna aluna)
        {
         
            if (ModelState.IsValid)
            {
                db.Entry(aluna).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
         
            return View(aluna);
        }

        // GET: Alunas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluna aluna = db.Alunas.Find(id);
            if (aluna == null)
            {
                return HttpNotFound();
            }
            return View(aluna);
        }

        // POST: Alunas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Aluna aluna = db.Alunas.Find(id);
            db.Alunas.Remove(aluna);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

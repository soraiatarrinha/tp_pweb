﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EstagiosDEIS.Models;
using Microsoft.AspNet.Identity;


namespace EstagiosDEIS.Controllers
{[AllowAnonymous]
    public class HomeController : Controller
    {
        private EstagiosContext db = new EstagiosContext();

        public ActionResult Index()
        {
            var propostas = db.Propostas.ToList();
            return View(propostas);
           
          
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }


       
    }
}
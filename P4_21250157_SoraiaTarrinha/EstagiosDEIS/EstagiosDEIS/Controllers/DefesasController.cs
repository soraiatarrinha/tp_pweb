﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstagiosDEIS.Models;

namespace EstagiosDEIS.Controllers
{[Authorize]
    public class DefesasController : Controller
    {
        private EstagiosContext db = new EstagiosContext();

        // GET: Defesas
        public ActionResult Index()
        {
            var defesas = db.Defesas.Include(d => d.Estagio);
            return View(defesas.ToList());
        }

        public ActionResult ListaDefesas()
        {
            var defesas = db.Defesas.Include(d => d.Estagio);
            return View(defesas.ToList());
        }

        // GET: Defesas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Defesa defesa = db.Defesas.Find(id);
            if (defesa == null)
            {
                return HttpNotFound();
            }
            return View(defesa);
        }

        // GET: Defesas/Create
        public ActionResult Create(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Estagio estagio = db.Estagios.Find(id);


            if (estagio == null)
                return HttpNotFound();
            return View();
        }

        // POST: Defesas/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "defesaId,EstagioId,Data_Defesa,avaliacao_Aluno")] Defesa defesa,int id)
        {
            Estagio estagio = db.Estagios.Find(id);

            defesa.EstagioId = estagio.estagioId;
            if (ModelState.IsValid)
            {
                db.Defesas.Add(defesa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(defesa);
        }

        // GET: Defesas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Defesa defesa = db.Defesas.Find(id);
            if (defesa == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstagioId = new SelectList(db.Estagios, "estagioId", "estagioId", defesa.EstagioId);
            return View(defesa);
        }

        // POST: Defesas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "defesaId,EstagioId,Data_Defesa,avaliacao_Aluno")] Defesa defesa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(defesa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstagioId = new SelectList(db.Estagios, "estagioId", "estagioId", defesa.EstagioId);
            return View(defesa);
        }

        // GET: Defesas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Defesa defesa = db.Defesas.Find(id);
            if (defesa == null)
            {
                return HttpNotFound();
            }
            return View(defesa);
        }

        // POST: Defesas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Defesa defesa = db.Defesas.Find(id);
            db.Defesas.Remove(defesa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult DefinirDataDefesa(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Defesa defesa = db.Defesas.Find(id);
            if (defesa == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstagioId = new SelectList(db.Estagios, "estagioId", "estagioId", defesa.EstagioId);
            return View(defesa);
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DefinirDataDefesa([Bind(Include = "defesaId,EstagioId,Data_Defesa,avaliacao_Aluno")] Defesa defesa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(defesa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstagioId = new SelectList(db.Estagios, "estagioId", "estagioId", defesa.EstagioId);
            return View(defesa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

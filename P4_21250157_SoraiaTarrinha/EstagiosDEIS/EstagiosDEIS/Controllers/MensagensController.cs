﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EstagiosDEIS.Models;
using Microsoft.AspNet.Identity;

namespace EstagiosDEIS.Controllers
{[Authorize]
    public class MensagensController : Controller
    {
        private EstagiosContext db = new EstagiosContext();


        public ActionResult Recebidas()
        {
            string userId = User.Identity.GetUserId();

            if (User.IsInRole("Aluno"))
            {
                Aluna al = db.Alunas.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Destinatario==al.Nome).ToList());

            }
            else if (User.IsInRole("Docente"))
            {
                var docente = db.Docentes.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Destinatario == docente.Nome).ToList());
            }
            else if (User.IsInRole("Empresa"))
            {
                var empresa = db.Empresas.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Destinatario == empresa.Nome).ToList());
            }
            else if (User.IsInRole("Comissao"))
            {
                var docente = db.Docentes.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Destinatario == docente.Nome).ToList());
            }

            return View(db.Mensagens.ToList());

        }


        public ActionResult Enviadas()
        {
            string userId = User.Identity.GetUserId();

            if (User.IsInRole("Aluno"))
            {
                Aluna al = db.Alunas.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Remetente == al.Nome).ToList());

            }
            else if (User.IsInRole("Docente"))
            {
                var docente = db.Docentes.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Remetente == docente.Nome).ToList());
            }
            else if (User.IsInRole("Empresa"))
            {
                var empresa = db.Empresas.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Mensagens.Where(c => c.Remetente == empresa.Nome).ToList());
            }

            return View(db.Mensagens.ToList());

        }

        // GET: Mensagens
        public ActionResult Index()
        {
           

            return View(db.Mensagens.ToList());
        }
        
        // GET: Mensagens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mensagens mensagens = db.Mensagens.Find(id);
            if (mensagens == null)
            {
                return HttpNotFound();
            }
            return View(mensagens);
        }

       
        // GET: Mensagens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mensagens mensagens = db.Mensagens.Find(id);
            if (mensagens == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlunaId = new SelectList(db.Alunas, "alunoId", "Nome", mensagens.AlunaId);
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", mensagens.DocenteId);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", mensagens.EmpresaId);
            return View(mensagens);
        }

        // POST: Mensagens/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "mensagemId,Remetente,Destinatario,AlunaId,DocenteId,EmpresaId,Assunto,Mensagem")] Mensagens mensagens)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mensagens).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlunaId = new SelectList(db.Alunas, "alunoId", "Nome", mensagens.AlunaId);
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", mensagens.DocenteId);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", mensagens.EmpresaId);
            return View(mensagens);
        }

        // GET: Mensagens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mensagens mensagens = db.Mensagens.Find(id);
            if (mensagens == null)
            {
                return HttpNotFound();
            }
            return View(mensagens);
        }

        // POST: Mensagens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mensagens mensagens = db.Mensagens.Find(id);
            db.Mensagens.Remove(mensagens);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [Authorize(Roles = "Aluno,Docente")]
        public ActionResult MensagemDocenteAluno(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Docente docente = db.Docentes.Find(id);


            if (docente == null)
                return HttpNotFound();
            return View();

        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MensagemDocenteAluno([Bind(Include = "mensagemId,Assunto,Mensagem,Destinatario,Remetente,AlunaId,DocenteId")] Mensagens mensagens,int id)
        {
           
           
               
                Docente doc = db.Docentes.Find(id);

          
            
                string userIdA = User.Identity.GetUserId();


                Aluna al = db.Alunas.SingleOrDefault(x => x.UserId == userIdA);
            mensagens.DocenteId = id;
            mensagens.AlunaId = al.alunoId;
            mensagens.Destinatario = doc.Nome;
            mensagens.Remetente = al.Nome;
            mensagens.EmpresaId = null;

            Session["AlunaId"] = mensagens.AlunaId.ToString();

            

            if (ModelState.IsValid)
            {
                db.Mensagens.Add(mensagens);
                db.SaveChanges();
                return RedirectToAction("Enviadas");
            }
            return View(mensagens);
        

               
           
        }
        [Authorize(Roles = "Aluno,Empresa")]
        public ActionResult MensagemEmpresaAluno(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresas.Find(id);


            if (empresa == null)
                return HttpNotFound();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MensagemEmpresaAluno([Bind(Include = "mensagemId,Assunto,Mensagem,Destinatario,Remetente,AlunaId,EmpresaId")] Mensagens mensagens,int id)
        {
           

          
            string userIdA = User.Identity.GetUserId();

            Empresa emp = db.Empresas.Find(id);
            Aluna al = db.Alunas.SingleOrDefault(x => x.UserId == userIdA);

            mensagens.DocenteId = null;
            mensagens.AlunaId = al.alunoId;
            mensagens.Destinatario = emp.Nome;
            mensagens.Remetente = al.Nome;
            mensagens.EmpresaId = id;

            Session["AlunaId"] = mensagens.AlunaId.ToString();



            if (ModelState.IsValid)
            {
                db.Mensagens.Add(mensagens);
                db.SaveChanges();
                return RedirectToAction("Enviadas");
            }
            return View(mensagens);
        }



        public ActionResult ResponderaoAluno(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluna aluna = db.Alunas.Find(id);


            if (aluna == null)
                return HttpNotFound();
            return View();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResponderaoAluno([Bind(Include = "mensagemId,Assunto,Mensagem,Destinatario,Remetente,AlunaId,DocenteId")] Mensagens mensagens, int id)
        {



            Aluna al = db.Alunas.Find(id);



            string userId = User.Identity.GetUserId();


            //Aluna al = db.Alunas.SingleOrDefault(x => x.UserId == userIdA);


              if (User.IsInRole("Docente"))
            {
                Docente docente = db.Docentes.SingleOrDefault(x => x.UserId.Equals(userId));
                mensagens.Remetente = docente.Nome;
                mensagens.DocenteId = docente.docenteId;
                mensagens.EmpresaId = null;
                Session["DocenteId"] = mensagens.DocenteId.ToString();
            }
            else if (User.IsInRole("Empresa"))
            {
                Empresa empresa = db.Empresas.SingleOrDefault(x => x.UserId.Equals(userId));
                mensagens.Remetente = empresa.Nome;
                mensagens.EmpresaId = empresa.empresaId;
                mensagens.DocenteId = null;
                Session["EmpresaId"] = mensagens.EmpresaId.ToString();
            }

           
            mensagens.AlunaId = id;
           
            mensagens.Destinatario = al.Nome;
          

            



            if (ModelState.IsValid)
            {
                db.Mensagens.Add(mensagens);
                db.SaveChanges();
                return RedirectToAction("Enviadas");
            }
            return View(mensagens);




        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstagiosDEIS.Models;
using Microsoft.AspNet.Identity;

namespace EstagiosDEIS.Controllers
{
    public class PropostasController : Controller
    {
        private EstagiosContext db = new EstagiosContext();
        [AllowAnonymous]
        public ActionResult ListaProjetosRamoSI()
        {
            var projetosramoSI = from r in db.Propostas
                                 where r.Ramo == TiposDeRamo.Sistemas_de_Informacao
                                 select r;


            return View(projetosramoSI);
        }
        [AllowAnonymous]
        public ActionResult ListaProjetosRamoDA()
        {
            var projetosramoDA = from r in db.Propostas
                                 where r.Ramo == TiposDeRamo.Desenvolvimento_de_Aplicacoes
                                 select r;


            return View(projetosramoDA);
        }

        [AllowAnonymous]
        public ActionResult ListaProjetosRamoR()
        {
            var projetosramoR = from r in db.Propostas
                                where r.Ramo == TiposDeRamo.Redes
                                select r;


            return View(projetosramoR);
        }


        [AllowAnonymous]
        public ActionResult ListaProjetosNorte()
        {
            var projetosramoN = from r in db.Propostas
                                where r.Regiao == Regiao.Norte
                                select r;


            return View(projetosramoN);
        }

        [AllowAnonymous]
        public ActionResult ListaProjetosCentro()
        {
            var projetosramoC = from r in db.Propostas
                                where r.Regiao == Regiao.Centro
                                select r;


            return View(projetosramoC);
        }

        [AllowAnonymous]
        public ActionResult ListaProjetosSul()
        {
            var projetosramoS = from r in db.Propostas
                                where r.Regiao == Regiao.Sul
                                select r;


            return View(projetosramoS);
        }

        [AllowAnonymous]
        public ActionResult ListaEmpresas()
        {
            var empresas = from e in db.Propostas
                           join c in db.Candidaturas on e.propostaId equals c.PropostaId
                           orderby e.Data.Year, e.Candidaturas.Count()
                           select e.Empresa;
            return View(empresas);
        }
        [Authorize]
        // GET: Propostas
        public ActionResult Index()
        {
            var propostas = db.Propostas.Include(p => p.Docente1).Include(p => p.Docente2).Include(p => p.Docente3).Include(p => p.Empresa);
            return View(propostas.ToList());
        }
        [Authorize(Roles ="Alunos")]
        public ActionResult IndexAlunos()
        {
            var propostas = db.Propostas.Include(p => p.Docente1).Include(p => p.Docente2).Include(p => p.Docente3).Include(p => p.Empresa);
            return View(propostas.ToList());
        }
        [Authorize(Roles="Comissao")]
        public ActionResult IndexComissao()
        {
            var propostas = db.Propostas.Include(p => p.Docente1).Include(p => p.Docente2).Include(p => p.Docente3).Include(p => p.Empresa);
            return View(propostas.ToList());
        }
        // GET: Propostas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proposta proposta = db.Propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        

        // GET: Propostas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proposta proposta = db.Propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId);
            ViewBag.DocenteId2 = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId2);
            ViewBag.DocenteId3 = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId3);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", proposta.EmpresaId);
            return View(proposta);
        }

        // POST: Propostas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "propostaId,Ramo,objetivos,condicoes_acesso,local,Regiao,Data,NumeroCandidaturas,CandidaturasAprovadas,DocenteId,EmpresaId,Aprovada,DocenteId2,DocenteId3")] Proposta proposta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proposta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId);
            ViewBag.DocenteId2 = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId2);
            ViewBag.DocenteId3 = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId3);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", proposta.EmpresaId);
            return View(proposta);
        }

        // GET: Propostas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proposta proposta = db.Propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        // POST: Propostas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proposta proposta = db.Propostas.Find(id);
            db.Propostas.Remove(proposta);
            db.SaveChanges();
            return RedirectToAction("Index");
        } 
        //[Authorize(Roles ="Docente,Admin")]
        public ActionResult CreatePropostaDocente()
        {
         
            ViewBag.DocenteId2 = new SelectList(db.Docentes, "docenteId", "Nome");
            ViewBag.DocenteId3 = new SelectList(db.Docentes, "docenteId", "Nome");
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome");

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePropostaDocente([Bind(Include = "propostaId,Ramo,objetivos,condicoes_acesso,local,Regiao,Data,NumeroCandidaturas,CandidaturasAprovadas,DocenteId,DocenteId2,DocenteId3,EmpresaId,Aprovada")] Proposta proposta)
        {
            proposta.Aprovada = false;
            proposta.CandidaturasAprovadas = 0;
            string userIdD = User.Identity.GetUserId();


            Docente docente = db.Docentes.SingleOrDefault(x => x.UserId == userIdD);
            proposta.DocenteId = docente.docenteId;
            if (ModelState.IsValid)
            {
                db.Propostas.Add(proposta);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

           
            ViewBag.DocenteId2 = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId2);
            ViewBag.DocenteId3 = new SelectList(db.Docentes, "docenteId", "Nome", proposta.DocenteId3);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", proposta.EmpresaId);

            return View(proposta);
        }

        // [Authorize(Roles = "Empresa,Admin")]

        public ActionResult CreatePropostaEmpresa()
        {

            

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePropostaEmpresa([Bind(Include = "propostaId,Ramo,objetivos,condicoes_acesso,local,Regiao,Data,NumeroCandidaturas,CandidaturasAprovadas,DocenteId,DocenteId2,DocenteId3,EmpresaId,Aprovada")] Proposta proposta)
        {

            proposta.Aprovada = false;
            proposta.CandidaturasAprovadas = 0;
            string userIdE = User.Identity.GetUserId();


            Empresa empresa = db.Empresas.SingleOrDefault(x => x.UserId == userIdE);
            proposta.EmpresaId = empresa.empresaId;

            if (ModelState.IsValid)
            {
                db.Propostas.Add(proposta);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            


            return View(proposta);
        }



        public ActionResult AprovarProposta(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Proposta proposta = db.Propostas.Find(id);


            if (proposta == null)
                return HttpNotFound();


            return View(proposta);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AprovarProposta([Bind(Include = "propostaId,Ramo,objetivos,condicoes_acesso,local,Regiao,Data,NumeroCandidaturas,CandidaturasAprovadas,DocenteId,DocenteId2,DocenteId3,EmpresaId,Aprovada")] Proposta proposta)
        {


            proposta.Aprovada = true;



            if (ModelState.IsValid)
            {
                db.Entry(proposta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proposta);

        }

        public ActionResult RecusarProposta(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Proposta proposta = db.Propostas.Find(id);


            if (proposta == null)
                return HttpNotFound();


            return View(proposta);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecusarProposta([Bind(Include = "propostaId,Ramo,objetivos,condicoes_acesso,local,Regiao,Data,NumeroCandidaturas,CandidaturasAprovadas,DocenteId,DocenteId2,DocenteId3,EmpresaId,Aprovada")] Proposta proposta)
        {


            proposta.Aprovada = false;



            if (ModelState.IsValid)
            {
                db.Entry(proposta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proposta);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstagiosDEIS.Models;
using Microsoft.AspNet.Identity;

namespace EstagiosDEIS.Controllers
{[Authorize]
    public class EstagiosController : Controller
    {
        private EstagiosContext db = new EstagiosContext();
        public ActionResult TotalEstagios(Estagio estagio)
        {
            var numeroEstagios = (from t in db.Estagios
                                  where t.dataFim != null
                                  orderby t.dataFim.Year
                                  select t).Count();


            return View(numeroEstagios);

        }
        // GET: Estagios
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            if (User.IsInRole("Docente") || User.IsInRole("Comissao"))
            {
                var docente = db.Docentes.SingleOrDefault(x => x.UserId.Equals(userId));


                return View(db.Estagios.Where(c => c.DocenteId== docente.docenteId).ToList());
            }
            else
            {
                var estagios = db.Estagios.Include(e => e.Aluna).Include(e => e.Docente).Include(e => e.Empresa).Include(e => e.Proposta);
                return View(estagios.ToList());
            }
        }

        // GET: Estagios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estagio estagio = db.Estagios.Find(id);
            if (estagio == null)
            {
                return HttpNotFound();
            }
            return View(estagio);
        }

        // GET: Estagios/Create
        public ActionResult Create(int? id)
        {


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Candidatura candidatura = db.Candidaturas.Find(id);


            if (candidatura == null)
                return HttpNotFound();
         

           
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome");
           
             return View();
        }

        // POST: Estagios/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "estagioId,AlunaId,DocenteId,EmpresaId,PropostaId,dataInicio,dataFim,avaliacao_Empresa,avaliacao_Aluno")] Estagio estagio,int id)
        {

            
            Candidatura candidatura = db.Candidaturas.Find(id);


            /*Para obter o id da proposta, do aluno e da empresa. Só é necessario escolher o docente que vai orientar o estágio*/
            Proposta proposta = db.Propostas.Find(candidatura.PropostaId);
            Aluna aluna = db.Alunas.Find(candidatura.AlunaId);
            Empresa empresa = db.Empresas.Find(proposta.EmpresaId);



            estagio.AlunaId = aluna.alunoId;
            estagio.PropostaId = proposta.propostaId;
            estagio.EmpresaId = empresa.empresaId;


            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", estagio.DocenteId);

            if (ModelState.IsValid)
            {
                db.Estagios.Add(estagio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(estagio);
        }

        // GET: Estagios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estagio estagio = db.Estagios.Find(id);
            if (estagio == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlunaId = new SelectList(db.Alunas, "alunoId", "Nome", estagio.AlunaId);
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", estagio.DocenteId);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", estagio.EmpresaId);
            ViewBag.PropostaId = new SelectList(db.Propostas, "propostaId", "propostaId", estagio.PropostaId);
            return View(estagio);
        }

        // POST: Estagios/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "estagioId,AlunaId,DocenteId,EmpresaId,PropostaId,dataInicio,dataFim,avaliacao_Empresa,avaliacao_Aluno")] Estagio estagio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estagio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlunaId = new SelectList(db.Alunas, "alunoId", "Nome", estagio.AlunaId);
            ViewBag.DocenteId = new SelectList(db.Docentes, "docenteId", "Nome", estagio.DocenteId);
            ViewBag.EmpresaId = new SelectList(db.Empresas, "empresaId", "Nome", estagio.EmpresaId);
            ViewBag.PropostaId = new SelectList(db.Propostas, "propostaId", "propostaId", estagio.PropostaId);
            return View(estagio);
        }

        // GET: Estagios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estagio estagio = db.Estagios.Find(id);
            if (estagio == null)
            {
                return HttpNotFound();
            }
            return View(estagio);
        }

        // POST: Estagios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Estagio estagio = db.Estagios.Find(id);
            db.Estagios.Remove(estagio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AvaliarAluno(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estagio estagio = db.Estagios.Find(id);
            if (estagio == null)
            {
                return HttpNotFound();
            }

            if (DateTime.Now < estagio.dataFim)
            {
                return View("Erro");
            }

            return View(estagio);
        }
        [HttpPost]
        public ActionResult AvaliarAluno([Bind(Include = "estagioId,AlunaId,DocenteId,EmpresaId,PropostaId,dataInicio,dataFim,avaliacao_Empresa,avaliacao_Aluno")]Estagio estagio,int id)
        {
            Estagio E = db.Estagios.Find(id);

            

            if (ModelState.IsValid)
            {
                db.Entry(estagio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estagio);
        }



        /*View de erro, para quando o estágio ainda não chegou ao fim*/
        public ActionResult Erro()
        {
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

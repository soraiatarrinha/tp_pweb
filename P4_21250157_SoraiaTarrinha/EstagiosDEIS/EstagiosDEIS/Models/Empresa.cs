﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EstagiosDEIS.Models
{
    [Table("Empresas")]

    public class Empresa
    {
        [Key]
        public int empresaId { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Localidade { get; set; }


      

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

       
    }
}
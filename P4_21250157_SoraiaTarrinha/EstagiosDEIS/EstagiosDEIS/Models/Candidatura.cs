﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EstagiosDEIS.Models
{
    [Table("Candidaturas")]
    public class Candidatura
    {
        [Key]
        public int candidaturaId { get; set; }

        [ForeignKey("Aluna")]
        public int AlunaId { get; set; }
        [ForeignKey("Proposta")]
        public int PropostaId { get; set; }
        public bool estado { get; set; } //aceite ou rejeitada 

        public  Aluna Aluna { get; set; }
        public  Proposta Proposta{ get; set; }
    }
}
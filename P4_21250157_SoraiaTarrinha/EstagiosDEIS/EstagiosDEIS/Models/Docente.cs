﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
namespace EstagiosDEIS.Models
{
    [Table("Docentes")]

    public class Docente
    {
        [Key]
        public int docenteId { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public int Contacto { get; set; }

    


        public IList<Proposta> Propostas{ get; set; }
      

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

       
    }
}
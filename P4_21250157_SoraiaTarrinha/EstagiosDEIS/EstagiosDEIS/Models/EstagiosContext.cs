﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace EstagiosDEIS.Models
{
    public class EstagiosContext:ApplicationDbContext
    {
        public EstagiosContext()
        {

        }
        public DbSet<Aluna> Alunas { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Docente> Docentes { get; set; }
        public DbSet<Estagio> Estagios { get; set; }
        public DbSet<Proposta> Propostas { get; set; }
        public DbSet<Defesa> Defesas { get; set; }
        public DbSet<Candidatura> Candidaturas { get; set; }
        public DbSet<Mensagens> Mensagens { get; set; }
       // public System.Data.Entity.DbSet<EstagiosDEIS.Models.ApplicationUser> applicationUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {


 base.OnModelCreating(modelBuilder);

        
        modelBuilder.Entity<Proposta>().HasRequired(i => i.Empresa).WithMany().WillCascadeOnDelete(false);


        modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
           
        }
    }

}
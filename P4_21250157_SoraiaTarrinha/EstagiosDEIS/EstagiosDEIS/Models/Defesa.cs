﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace EstagiosDEIS.Models
{
    [Table("Defesas")]

    public class Defesa
    {
        [Key]
        public int defesaId { get; set; }

        [ForeignKey("Estagio")]
        public int EstagioId { get; set; }

     
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime Data_Defesa { get; set; }

        [Range(0, 20, ErrorMessage = "Avaliacao tem de ser de 0 a 20")]
        public double avaliacao_Aluno { get; set; }

        public  Estagio Estagio { get; set; }
    }
}
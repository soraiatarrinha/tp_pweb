﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace EstagiosDEIS.Models
{
    [Table("Propostas")]

    public class Proposta
    {
        [Key]
        public int propostaId { get; set; }
        [Required]
        public TiposDeRamo? Ramo { get; set; }
        [Required]
        public string objetivos { get; set; }
        [Required]
        public string condicoes_acesso { get; set; }
        [Required]
        public string local { get; set; }

        public Regiao  Regiao { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime Data { get; set; }
        
        [Required]
        public int NumeroCandidaturas { get; set; }
        public int CandidaturasAprovadas { get; set; }
    
        [ForeignKey("Docente1")]
        public int? DocenteId { get; set; }

        [ForeignKey("Empresa")]
        public int EmpresaId { get; set; }

        public bool Aprovada { get; set; }

        public string Justificacao { get; set; }

        [ForeignKey("Docente2")]
        public int? DocenteId2 { get; set; }

        [ForeignKey("Docente3")]
        public int? DocenteId3 { get; set; }
   

        public Empresa Empresa { get; set; }
        public Docente Docente1 { get; set; }
        public Docente Docente2 { get; set; }
        public Docente Docente3 { get; set; }
        public IList<Candidatura> Candidaturas { get; set; }
    }
}
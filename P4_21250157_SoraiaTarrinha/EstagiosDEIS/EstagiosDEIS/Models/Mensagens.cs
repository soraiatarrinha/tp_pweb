﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
namespace EstagiosDEIS.Models
{
    [Table("Mensagens")]
    public class Mensagens
    {
        [Key]
        public int mensagemId { get; set; }

        
        public string Remetente { get; set; }

        
        public string Destinatario { get; set; }

        [ForeignKey("Aluna")]
        public int AlunaId { get; set; }

        [ForeignKey("Docente")]
        public int? DocenteId { get; set; }

        [ForeignKey("Empresa")]
        public int? EmpresaId { get; set; }




        [Required(ErrorMessage = "Escreva o assunto")]
        [DisplayName("Assunto")]
        [StringLength(100, MinimumLength = 5)]
        [DataType(DataType.Text)]
        public string Assunto { get; set; }

        [Required(ErrorMessage = "Escreva a sua mensagem")]
        [DisplayName("Mensagem")]
        [StringLength(5000, MinimumLength = 20)]
        [DataType(DataType.MultilineText)]
        public string Mensagem { get; set; }



        public  Aluna Aluna { get; set; }
        public  Docente Docente { get; set; }
        public  Empresa Empresa { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EstagiosDEIS.Models
{
    [Table("Alunas")]
    public class Aluna
    {
        [Key]
        public int alunoId { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public int Contacto { get; set; }
        [Required]
        public int Ano { get; set; }
        [Required]
        public TiposDeRamo? Ramo { get; set; }
        [Required]
        public string Disciplinas { get; set; }
        [Required]
        public int Notas { get; set; }

        public int num_candidaturas { get; set; }


        public IList<Candidatura> Candidaturas { get; set; }
       


        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

      
    }
}
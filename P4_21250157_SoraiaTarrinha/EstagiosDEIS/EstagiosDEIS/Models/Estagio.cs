﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EstagiosDEIS.Models
{
    [Table("Estagios")]

    public class Estagio
    {
        [Key]
        public int estagioId { get; set; }

        [ForeignKey("Aluna")]
        public int AlunaId { get; set; }

        [ForeignKey("Docente")]
        public int DocenteId { get; set; }    //orientador de estágio 

        [ForeignKey("Empresa")]
        public int EmpresaId { get; set; }

        [ForeignKey("Proposta")]
        public int PropostaId { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime dataInicio { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime dataFim { get; set; }
        
        [Range(0, 20, ErrorMessage = "Nota tem de ser de 0 a 20")]
        public double avaliacao_Empresa { get; set; }
        [Range(0, 20, ErrorMessage = "Nota tem de ser de 0 a 20")]
        public double avaliacao_Aluno { get; set; }

        public  Proposta Proposta { get; set; }
        public  Aluna Aluna { get; set; } 
        public Docente Docente { get; set; }
        public  Empresa Empresa { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstagiosDEIS.Models
{
    public enum TiposDeRamo
    {
        Sistemas_de_Informacao=1,
        Desenvolvimento_de_Aplicacoes=2,
        Redes=3
    }
}
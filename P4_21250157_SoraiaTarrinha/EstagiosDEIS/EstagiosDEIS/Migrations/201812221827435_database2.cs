namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class database2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Aluno", newName: "Alunas");
            RenameColumn(table: "dbo.Candidaturas", name: "AlunoId", newName: "AlunaId");
            RenameColumn(table: "dbo.Mensagens", name: "AlunoId", newName: "AlunaId");
            RenameColumn(table: "dbo.Estagios", name: "AlunoId", newName: "AlunaId");
            RenameIndex(table: "dbo.Candidaturas", name: "IX_AlunoId", newName: "IX_AlunaId");
            RenameIndex(table: "dbo.Mensagens", name: "IX_AlunoId", newName: "IX_AlunaId");
            RenameIndex(table: "dbo.Estagios", name: "IX_AlunoId", newName: "IX_AlunaId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Estagios", name: "IX_AlunaId", newName: "IX_AlunoId");
            RenameIndex(table: "dbo.Mensagens", name: "IX_AlunaId", newName: "IX_AlunoId");
            RenameIndex(table: "dbo.Candidaturas", name: "IX_AlunaId", newName: "IX_AlunoId");
            RenameColumn(table: "dbo.Estagios", name: "AlunaId", newName: "AlunoId");
            RenameColumn(table: "dbo.Mensagens", name: "AlunaId", newName: "AlunoId");
            RenameColumn(table: "dbo.Candidaturas", name: "AlunaId", newName: "AlunoId");
            RenameTable(name: "dbo.Alunas", newName: "Aluno");
        }
    }
}

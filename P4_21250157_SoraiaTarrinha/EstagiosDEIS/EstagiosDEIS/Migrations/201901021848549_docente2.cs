namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class docente2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Propostas", "DocenteId", "dbo.Docentes");
            DropIndex("dbo.Propostas", new[] { "DocenteId" });
            AlterColumn("dbo.Propostas", "DocenteId", c => c.Int());
            CreateIndex("dbo.Propostas", "DocenteId");
            AddForeignKey("dbo.Propostas", "DocenteId", "dbo.Docentes", "docenteId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Propostas", "DocenteId", "dbo.Docentes");
            DropIndex("dbo.Propostas", new[] { "DocenteId" });
            AlterColumn("dbo.Propostas", "DocenteId", c => c.Int(nullable: false));
            CreateIndex("dbo.Propostas", "DocenteId");
            AddForeignKey("dbo.Propostas", "DocenteId", "dbo.Docentes", "docenteId", cascadeDelete: true);
        }
    }
}

namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class propostajustificacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Propostas", "Justificacao", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Propostas", "Justificacao");
        }
    }
}

namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modeloMensagens : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Mensagens", "Remetente", c => c.String());
            AlterColumn("dbo.Mensagens", "Destinatario", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Mensagens", "Destinatario", c => c.String(nullable: false));
            AlterColumn("dbo.Mensagens", "Remetente", c => c.String(nullable: false));
        }
    }
}

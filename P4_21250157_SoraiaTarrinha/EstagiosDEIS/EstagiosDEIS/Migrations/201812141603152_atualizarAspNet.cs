namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class atualizarAspNet : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "UserRole");
            DropColumn("dbo.Docentes", "Comissao");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Docentes", "Comissao", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "UserRole", c => c.String());
        }
    }
}

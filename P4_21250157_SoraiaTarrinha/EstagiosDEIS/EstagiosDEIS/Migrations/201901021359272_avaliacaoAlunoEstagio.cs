namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class avaliacaoAlunoEstagio : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Estagios", "avaliacao_Aluno", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estagios", "avaliacao_Aluno");
        }
    }
}

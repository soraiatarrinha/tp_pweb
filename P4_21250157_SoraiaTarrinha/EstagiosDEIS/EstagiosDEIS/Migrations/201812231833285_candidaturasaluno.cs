namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class candidaturasaluno : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Alunas", "num_candidaturas", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Alunas", "num_candidaturas");
        }
    }
}

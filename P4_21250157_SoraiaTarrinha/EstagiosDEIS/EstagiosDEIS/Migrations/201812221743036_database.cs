namespace EstagiosDEIS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class database : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alunos",
                c => new
                    {
                        alunoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        Contacto = c.Int(nullable: false),
                        Ano = c.Int(nullable: false),
                        Ramo = c.Int(nullable: false),
                        Disciplinas = c.String(nullable: false),
                        Notas = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.alunoId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Candidaturas",
                c => new
                    {
                        candidaturaId = c.Int(nullable: false, identity: true),
                        AlunoId = c.Int(nullable: false),
                        PropostaId = c.Int(nullable: false),
                        estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.candidaturaId)
                .ForeignKey("dbo.Alunos", t => t.AlunoId, cascadeDelete: true)
                .ForeignKey("dbo.Propostas", t => t.PropostaId, cascadeDelete: true)
                .Index(t => t.AlunoId)
                .Index(t => t.PropostaId);
            
            CreateTable(
                "dbo.Propostas",
                c => new
                    {
                        propostaId = c.Int(nullable: false, identity: true),
                        Ramo = c.Int(nullable: false),
                        objetivos = c.String(nullable: false),
                        condicoes_acesso = c.String(nullable: false),
                        local = c.String(nullable: false),
                        Regiao = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false),
                        NumeroCandidaturas = c.Int(nullable: false),
                        CandidaturasAprovadas = c.Int(nullable: false),
                        DocenteId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        Aprovada = c.Boolean(nullable: false),
                        DocenteId2 = c.Int(),
                        DocenteId3 = c.Int(),
                        Docente_docenteId = c.Int(),
                    })
                .PrimaryKey(t => t.propostaId)
                .ForeignKey("dbo.Docentes", t => t.Docente_docenteId)
                .ForeignKey("dbo.Docentes", t => t.DocenteId, cascadeDelete: true)
                .ForeignKey("dbo.Docentes", t => t.DocenteId2)
                .ForeignKey("dbo.Docentes", t => t.DocenteId3)
                .ForeignKey("dbo.Empresas", t => t.EmpresaId)
                .Index(t => t.DocenteId)
                .Index(t => t.EmpresaId)
                .Index(t => t.DocenteId2)
                .Index(t => t.DocenteId3)
                .Index(t => t.Docente_docenteId);
            
            CreateTable(
                "dbo.Docentes",
                c => new
                    {
                        docenteId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        Contacto = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.docenteId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Mensagens",
                c => new
                    {
                        mensagemId = c.Int(nullable: false, identity: true),
                        Remetente = c.String(nullable: false),
                        Destinatario = c.String(nullable: false),
                        AlunoId = c.Int(nullable: false),
                        DocenteId = c.Int(),
                        EmpresaId = c.Int(),
                        Assunto = c.String(nullable: false, maxLength: 100),
                        Mensagem = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.mensagemId)
                .ForeignKey("dbo.Alunos", t => t.AlunoId, cascadeDelete: true)
                .ForeignKey("dbo.Docentes", t => t.DocenteId)
                .ForeignKey("dbo.Empresas", t => t.EmpresaId)
                .Index(t => t.AlunoId)
                .Index(t => t.DocenteId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        empresaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        Localidade = c.String(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.empresaId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Defesas",
                c => new
                    {
                        defesaId = c.Int(nullable: false, identity: true),
                        EstagioId = c.Int(nullable: false),
                        Data_Defesa = c.DateTime(nullable: false),
                        avaliacao_Aluno = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.defesaId)
                .ForeignKey("dbo.Estagios", t => t.EstagioId, cascadeDelete: true)
                .Index(t => t.EstagioId);
            
            CreateTable(
                "dbo.Estagios",
                c => new
                    {
                        estagioId = c.Int(nullable: false, identity: true),
                        AlunoId = c.Int(nullable: false),
                        DocenteId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        PropostaId = c.Int(nullable: false),
                        dataInicio = c.DateTime(nullable: false),
                        dataFim = c.DateTime(nullable: false),
                        avaliacao_Empresa = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.estagioId)
                .ForeignKey("dbo.Alunos", t => t.AlunoId, cascadeDelete: true)
                .ForeignKey("dbo.Docentes", t => t.DocenteId, cascadeDelete: true)
                .ForeignKey("dbo.Empresas", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Propostas", t => t.PropostaId, cascadeDelete: false)
                .Index(t => t.AlunoId)
                .Index(t => t.DocenteId)
                .Index(t => t.EmpresaId)
                .Index(t => t.PropostaId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Defesas", "EstagioId", "dbo.Estagios");
            DropForeignKey("dbo.Estagios", "PropostaId", "dbo.Propostas");
            DropForeignKey("dbo.Estagios", "EmpresaId", "dbo.Empresas");
            DropForeignKey("dbo.Estagios", "DocenteId", "dbo.Docentes");
            DropForeignKey("dbo.Estagios", "AlunoId", "dbo.Alunos");
            DropForeignKey("dbo.Candidaturas", "PropostaId", "dbo.Propostas");
            DropForeignKey("dbo.Propostas", "EmpresaId", "dbo.Empresas");
            DropForeignKey("dbo.Propostas", "DocenteId3", "dbo.Docentes");
            DropForeignKey("dbo.Propostas", "DocenteId2", "dbo.Docentes");
            DropForeignKey("dbo.Propostas", "DocenteId", "dbo.Docentes");
            DropForeignKey("dbo.Propostas", "Docente_docenteId", "dbo.Docentes");
            DropForeignKey("dbo.Mensagens", "EmpresaId", "dbo.Empresas");
            DropForeignKey("dbo.Empresas", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Mensagens", "DocenteId", "dbo.Docentes");
            DropForeignKey("dbo.Mensagens", "AlunoId", "dbo.Alunos");
            DropForeignKey("dbo.Docentes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Candidaturas", "AlunoId", "dbo.Alunos");
            DropForeignKey("dbo.Alunos", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Estagios", new[] { "PropostaId" });
            DropIndex("dbo.Estagios", new[] { "EmpresaId" });
            DropIndex("dbo.Estagios", new[] { "DocenteId" });
            DropIndex("dbo.Estagios", new[] { "AlunoId" });
            DropIndex("dbo.Defesas", new[] { "EstagioId" });
            DropIndex("dbo.Empresas", new[] { "UserId" });
            DropIndex("dbo.Mensagens", new[] { "EmpresaId" });
            DropIndex("dbo.Mensagens", new[] { "DocenteId" });
            DropIndex("dbo.Mensagens", new[] { "AlunoId" });
            DropIndex("dbo.Docentes", new[] { "UserId" });
            DropIndex("dbo.Propostas", new[] { "Docente_docenteId" });
            DropIndex("dbo.Propostas", new[] { "DocenteId3" });
            DropIndex("dbo.Propostas", new[] { "DocenteId2" });
            DropIndex("dbo.Propostas", new[] { "EmpresaId" });
            DropIndex("dbo.Propostas", new[] { "DocenteId" });
            DropIndex("dbo.Candidaturas", new[] { "PropostaId" });
            DropIndex("dbo.Candidaturas", new[] { "AlunoId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Alunos", new[] { "UserId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Estagios");
            DropTable("dbo.Defesas");
            DropTable("dbo.Empresas");
            DropTable("dbo.Mensagens");
            DropTable("dbo.Docentes");
            DropTable("dbo.Propostas");
            DropTable("dbo.Candidaturas");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Alunos");
        }
    }
}

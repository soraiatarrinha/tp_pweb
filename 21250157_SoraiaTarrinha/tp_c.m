function tp_c(rede)


load(rede);

Formas3_Circle = dir('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\circle\*.png');
Formas3_Square=dir('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\square\*.png');
Formas3_Star=dir('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\star\*.png');
Formas3_Triangle=dir('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\triangle\*.png');
nC=length(Formas3_Circle);
nSq=length(Formas3_Square);
nSt=length(Formas3_Star);
nT=length(Formas3_Triangle);
nTotal=nC+nSq+nSt+nT;
tam=45;
inputs = zeros(tam*tam, nTotal);
circles=zeros(tam*tam,nC);
squares=zeros(tam*tam,nSq);
stars=zeros(tam*tam,nSt); 
triangles=zeros(tam*tam,nT);
    for c=1:nC
      filename = strcat('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\circle\',Formas3_Circle(c).name);
      x = imread(filename);
      
      b=imresize(x,[tam tam]);
     
      circles(:,c)=imbinarize(b(:));
      
    end
   for s=1:nSq
       
      filename = strcat('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\square\',Formas3_Square(s).name);
      x = imread(filename);
      
      b=imresize(x,[tam tam]);
     
      squares(:,s)=imbinarize(b(:));
   end

for st=1:nSt
filename = strcat('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\star\',Formas3_Square(st).name);
      x = imread(filename);
      
      b=imresize(x,[tam tam]);
     
      stars(:,s)=imbinarize(b(:));


end

for t=1:nT
filename = strcat('D:\OneDrive - ISEC\4 Ano\2 Semestre\CR\TP CR\TemaRN_Imagens_\Formas_3\triangle\',Formas3_Triangle(t).name);
      x = imread(filename);
      
      b=imresize(x,[tam tam]);
     
      triangles(:,t)=imbinarize(b(:));
end

%Para colocar todas as imagens numa s� matriz de inputs 

for tot=1:nTotal %n� total de imagens 
  if tot<51
    for c=1:nC
  
    inputs(:,tot)=circles(:,c);
   
    end
  
  else if tot<101
     for s=1:nSq
        inputs(:,tot)=squares(:,s);
     end
  
  else if tot<151
   for st=1:nSt   
            inputs(:,tot)=stars(:,st);
   end
  
      else 
      for t=1:nT    
   inputs(:,tot)=triangles(:,t);
      end
      end
      end
  end
end

Targets=zeros(1,nTotal);
for i = 1:nTotal
    if i<51
        Targets(1,i)=1;
    else if i<101
            Targets(2,i)=1;
        else if i<151
                Targets(3,i)=1;
            else
                Targets(4,i)=1;
            end
        end
    end
end



  
 


out=net(inputs);

plotconfusion(Targets, out);


set(gca,'xticklabel',{'Circulo' 'Quadrado' 'Estrela' 'Triangulo' 'Total'})
set(gca,'yticklabel',{'Circulo' 'Quadrado' 'Estrela' 'Triangulo' 'Total'})
end

